var router = require('express').Router();
const { requiresAuth } = require('express-openid-connect');
const { body, validationResult } = require('express-validator');
const db = require('../database');
var server = require('../server.js');

var bodyParser = require('body-parser');
var multer = require('multer');
const { render } = require('ejs');
var upload = multer();

router.get('/', function (req, res, next) {
  res.render('lab2-odabir')
  block = true;
  loggedInUser = ':)'
});


router.post('/login',
  [
    body('vrstaNapada')
  ],
  async function (req, res, next) {
  console.log(req.body.vrstaNapada)

  let vrstaNapada = req.body.vrstaNapada;

  if(vrstaNapada == 1){
    stanjeBazePrije = (await db.query('select * from users_lab2 order by userid')).rows;
    res.render('lab2-login', {
      greska: '',
      napad: 1,
      stanjeBazePrije : stanjeBazePrije,
      stanjeBazeNakon : stanjeBazePrije
    });

  } else if(vrstaNapada == 2){
    block = false;
    res.render('lab2-login', {
      greska: '',
      napad: 2
    });

  } else {
    res.render('lab2-login', {
      greska: '',
      napad: 0
    });

  }
});

router.post('/profile', 
  [
    body('username'),
    body('password'),
    body('vrstaNapada')
  ],
  async function (req, res, next){

    let greksa = 0;

    if(!unosOk(req.body.username)){
      res.render('lab2-login', {
        greska: 'Username ili password su neispravni.',
        napad: req.body.vrstaNapada
      });
    }

    lozinka = (await db.query('select password from users_lab2 where username= $1', [req.body.username])).rows

    podaci = (await db.query('select * from users_lab2 where username= $1', [req.body.username])).rows[0]

    if(lozinka === undefined || lozinka[0] === undefined){
      greska = 1;
      res.render('lab2-login', {
        greska: 'Username ili password su neispravni.',
        napad: req.body.vrstaNapada
      });
    }

    if(req.body.password === lozinka[0].password){
      let podaciZaPrikaz = [];
      podaciZaPrikaz.push(podaci.username, podaci.profilepicture, podaci.birthdate, podaci.address);
      console.log(podaciZaPrikaz)

      res.render('lab2-profil', {
        username: req.body.username,
        napad: req.body.vrstaNapada,
        podaci: podaciZaPrikaz
      })
    } else {
      greska = 1;
      res.render('lab2-login', {
        greska: 'Username ili password su neispravni.',
        napad: req.body.vrstaNapada
      });
    }
  
});

function unosOk(unos){
  let regex = /^[a-zA-Z0-9]+$/
  if (regex.test(String(unos)) == false){
    console.log("neispravan unos");
    return true;
  } else {
    console.log("ispravan unos");
    return true;
  }
}

router.post('/profileSQLInjection', 
  [
    body('username'),
    body('password'),
    body('vrstaNapada')
  ],
  async function (req, res, next){

    let greksa = 0;

    console.log(req.body.username);

    stanjeBazePrije = (await db.query('select * from users_lab2 order by userid')).rows;

    let query = `SELECT password FROM users_lab2 WHERE username = '${req.body.username}'`;
    try {
      lozinka = (await db.query(query)).rows;
    } catch (error) {
      lozinka = undefined;
    }

    podaci = (await db.query('select * from users_lab2 where username= $1', [req.body.username])).rows[0]

    stanjeBazeNakon = (await db.query('select * from users_lab2 order by userid')).rows;
    console.log(stanjeBazeNakon)
    console.log(lozinka)

    if(lozinka == undefined || lozinka[0] === undefined){
      greska = 1;
      res.render('lab2-login', {
        greska: 'Username ili password su neispravni.',
        napad: req.body.vrstaNapada,
        stanjeBazePrije: stanjeBazePrije,
        stanjeBazeNakon: stanjeBazeNakon
      });
    }

    if(req.body.password === lozinka[0].password){
      let podaciZaPrikaz = [];
      podaciZaPrikaz.push(podaci.username, podaci.profilepicture, podaci.birthdate, podaci.address);

      res.render('lab2-profil', {
        username: req.body.username,
        napad: req.body.vrstaNapada,
        podaci: podaciZaPrikaz
      })
    } else {
      greska = 1;
      res.render('lab2-login', {
        greska: 'Username ili password su neispravni.',
        napad: req.body.vrstaNapada,
        stanjeBazePrije: stanjeBazePrije,
        stanjeBazeNakon: stanjeBazeNakon
      });
    }
  
});

router.post('/profileBAC', async function (req, res, next){
  userId = (await db.query('select userid from users_lab2 where username= $1', [req.body.username])).rows
  lozinka = (await db.query('select password from users_lab2 where username= $1', [req.body.username])).rows
  console.log(userId)
  block = false;
  loggedInUser = req.body.username;


  if(lozinka === undefined || lozinka[0] === undefined){
    greska = 1;
    res.render('lab2-login', {
      greska: 'Username ili password su neispravni.',
      napad: req.body.vrstaNapada
    });
  }

  if(req.body.password === lozinka[0].password){
    res.redirect('/profileBAC2?userId='+ userId[0].userid)
  } else {
    greska = 1;
    res.render('lab2-login', {
      greska: 'Username ili password su neispravni.',
      napad: req.body.vrstaNapada
    });
  }
});

var loggedInUser = ':)';
var block = true;
var blockRouting = function(req, res, next) {
  if (block === true)
    res.render('lab2-neovlasten')
  next();
};

router.get('/profileBAC2', blockRouting, async function (req, res, next){
  var param = req.query.userId;
  podaci = (await db.query('select * from users_lab2 where userid= $1', [param])).rows[0]

  let podaciZaPrikaz = [];
  podaciZaPrikaz.push(podaci.username, podaci.profilepicture, podaci.birthdate, podaci.address);

  res.render('lab2-profil', {
    username: loggedInUser,
    napad: 2,
    podaci: podaciZaPrikaz
  });
});

router.post('/findUser', async function (req, res, next){
  podaci = (await db.query('select username, profilepicture from users_lab2 where username= $1', [req.body.username])).rows
  res.render('findUser', {
    napad: 0,
    podaci: podaci
  })

});

router.post('/findUserSQLInjection', async function (req, res, next){

  let query = `SELECT username, profilepicture FROM users_lab2 WHERE username = '${req.body.username}'`;
    try {
      podaci = (await db.query(query)).rows;
    } catch (error) {
      podaci = [];
    }
    console.log(podaci)

  res.render('findUser', {
    napad: 1,
    podaci: podaci
  });

});


module.exports = router;
