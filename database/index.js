const {Pool} = require('pg');

const pool = new Pool({
    user: 'web2_lab1_45oj_user',
    host: 'dpg-cdeje2kgqg4d3ggounug-a',
    database: 'web2_lab1_45oj',
    password: 'OUILDiPJ0A5lvCWToj0M23k4vqbKB8kp',
    port: 5432
    //ssl: true
});

module.exports = {
    query: (text, params) => {
        const start = Date.now();
        return pool.query(text, params)
            .then(res => {
                const duration = Date.now() - start;
                //console.log('executed query', {text, params, duration, rows: res.rows});
                return res;
            });
    }
}