const {Pool} = require('pg');

const pool = new Pool({
    user: 'web2_lab1_45oj_user',
    host: 'dpg-cdeje2kgqg4d3ggounug-a',
    database: 'web2_lab1_45oj',
    password: 'OUILDiPJ0A5lvCWToj0M23k4vqbKB8kp',
    port: 5432
    //ssl: true
});

const sql_create_users = `
    DROP TABLE IF EXISTS users_lab2;
    CREATE TABLE users_lab2 (
    userId int PRIMARY KEY,
    email text NOT NULL UNIQUE,
    username text NOT NULL UNIQUE,
    password text NOT NULL,
    profilePicture text NOT NULL,
    address text NOT NULL,
    birthDate text NOT NULL,
    role int NOT NULL
)`;

const sql_insert_users = `
    INSERT INTO users_lab2(
    userId, email, username, password, profilePicture, address, birthDate, role)
    VALUES 
    (1, '59d5s.admin@inbox.testmail.app', 'admin', '123456', 'https://cdn.pixabay.com/photo/2016/12/07/21/01/cartoon-1890438_960_720.jpg', 'ulica a', '1.1.1970.', 1),
    (2, '59d5s.ana@inbox.testmail.app', 'ana', 'ana123',  'https://cdn.pixabay.com/photo/2017/03/01/22/18/avatar-2109804_960_720.png', 'ulica b', '20.12.1980.', 2),
    (3, '59d5s.pero@inbox.testmail.app', 'pero', 'pero123',  'https://cdn.pixabay.com/photo/2018/08/28/13/29/avatar-3637561_960_720.png', 'ulica c', '28.4.1992.', 2),
    (5, '59d5s.iva@inbox.testmail.app', 'iva', 'iva123', 'https://cdn.pixabay.com/photo/2018/08/28/14/09/avatar-3637645_960_720.png', 'ulica d', '03.09.1995.', 2),
	(6, '59d5s.luka@inbox.testmail.app', 'luka', 'luka123', 'https://cdn.pixabay.com/photo/2016/03/31/19/56/avatar-1295399_960_720.png', 'ulica e', '19.2.2002.', 2),
	(7, '59d5s.maja@inbox.testmail.app', 'maja', 'maja123', 'https://cdn.pixabay.com/photo/2020/06/21/06/00/bohemian-5323332_960_720.png', 'ulica f', '13.06.1985.', 2),
	(8, '59d5s.marko@inbox.testmail.app', 'marko', 'marko123', 'https://cdn.pixabay.com/photo/2017/02/23/13/05/avatar-2092113_960_720.png', 'ulica g', '11.10.1988.', 2)
`;

pool.query(sql_create_users, [], (err, result) => {
    if (err) {
        return console.error(err.message);
    }
    console.log("Successful creation of the 'users_lab2' table");
    pool.query(sql_insert_users, [], (err, result) => {
        if (err) {
            return console.error(err.message);
        }
    });
});
