const dotenv = require('dotenv');
const express = require('express');
const http = require('http');
const logger = require('morgan');
const path = require('path');
const router = require('./routes/index');
const { auth } = require('express-openid-connect');
const db = require('./database');

var bodyParser = require('body-parser');
var multer = require('multer');
var upload = multer();

dotenv.load();

const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// for parsing application/json
app.use(bodyParser.json()); 

// for parsing application/xwww-
app.use(bodyParser.urlencoded({ extended: true })); 
app.use(express.urlencoded({extended: true}));
//form-urlencoded

// for parsing multipart/form-data
app.use(upload.array()); 

app.use(logger('dev'));
app.use(express.static(path.join(__dirname, '/static')));
app.use(express.json());

app.use('/', router);

var block = false;
var blockRouting = function(req, res, next) {
  if (block === true)
    return res.send(503); // 'Service Unavailable'
  next();
};

//----------------------------------------------------------------------------------------

const externalUrl = process.env.RENDER_EXTERNAL_URL;
const port = externalUrl && process.env.PORT ? parseInt(process.env.PORT) : 4080;


//const port = process.env.PORT || 3000;
if (externalUrl) {
  console.log("----------------");
  console.log(externalUrl)
  const hostname = '127.0.0.1';
  app.listen(port, hostname, () => {
  console.log(`Server locally running at http://${hostname}:${port}/ and from
  outside on ${externalUrl}`);
  });
} else {
  https.createServer({
  key: fs.readFileSync('server.key'),
  cert: fs.readFileSync('server.cert')
  }, app)
  .listen(port, function () {
  console.log(`Server running at https://localhost:${port}/`);
  });
}

const config = {
  authRequired: false,
  auth0Logout: true,
  baseURL: externalUrl || `https://localhost:${port}`
};


//----------------------------------------------------------------------------------------

module.exports = {
  blockRouting : blockRouting,
  block : block
}
